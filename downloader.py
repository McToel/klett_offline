import requests
import shutil
from bs4 import BeautifulSoup
from config import login_credentials

# Lambacher Schweizer Kursstufe
BASE_URL = 'https://bridge.klett.de/PPL-QVWS6AYT8X/content/pages/page_{}/Scale2.png'

# login to klett.de
k_down = requests.Session()
login_page = k_down.get('https://klett.de/login')
soup = BeautifulSoup(login_page.text, 'html.parser')
login_form = soup.find(id='kc-form-login')
login_link = login_form['action']
login = k_down.post(login_link, data=login_credentials)

# Download book
i = 0
while True:
    print('loading page {}'.format(i))
    response = k_down.get(BASE_URL.format(i), stream=True)
    if response.status_code == 200:
        with open('book/page_{}.png'.format(i), 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
            i += 1
    else:
        break
